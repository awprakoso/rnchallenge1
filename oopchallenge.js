//OOP Challenge

class vehicle {
    constructor(type, color, speed){
        this.vehicletype = type;
        this.vehiclecolor = color;
        this.vehiclespeed = speed;
    }
    present(){
        return 'I have a ' + this.vehicletype + ', it is ' + this.vehiclecolor + ', and it can run up to ' + this.vehiclespeed + '.'
    }
}

class car extends vehicle {
    constructor(type, color, speed){
        super(type, color, speed);
    }
    show(){
        return this.present()
    }
}

class motorbike extends vehicle {
    constructor(type, color, speed, wheel){
        super(type, color, speed);
        this.motorbikewheel = wheel
    }
    show(){
        return this.present() + ' It has ' + this.motorbikewheel + ' wheel(s)'
    }
}

myCar = new car("car", "red", "250 km/h");
myBike = new motorbike("motorbike", "blue", "125 km/h", "2")

console.log(myCar.show());
console.log(myBike.show());